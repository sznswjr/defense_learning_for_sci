# coding=utf-8

from enum import Enum

import torch
import torch.nn as nn

from torch.autograd import Variable
from torchvision import datasets, transforms
from models import CNNModel, AlexNet, VGG

# 输出结果目录名
RESULTDIR = "./RESULT/"
# 加载规避点目录名
MODELDIR = "./MODEL/"

'''
模型使用方法：

model = CNNModel()
'''

# 数据集模式
class DatasetMode(Enum):
    # 原始数据集
    ORIGIN = 0
    # 交换标签1
    EXCHANGE1 = 1
    # 交换标签7
    EXCHANGE7 = 2

    # 交换标签5
    EXCHANGE5 = 3
    # 交换标签6
    EXCHANGE6 = 4

    # 交换标签4
    EXCHANGE4 = 5
    # 交换标签9
    EXCHANGE9 = 6

    # 概率交换标签
    EXCHANGEPROB = 7

# 按需求生成的数据集
class MNISTDataset(torch.utils.data.Dataset):
    # 数据集模式对应的标签更改
    definelabels = {
        DatasetMode.EXCHANGE1 : torch.tensor([0, 7, 2, 3, 4, 5, 6, 7, 8, 9]),
        DatasetMode.EXCHANGE7: torch.tensor([0, 1, 2, 3, 4, 5, 6, 1, 8, 9]),
        DatasetMode.EXCHANGE5: torch.tensor([0, 1, 2, 3, 4, 6, 6, 7, 8, 9]),
        DatasetMode.EXCHANGE6: torch.tensor([0, 1, 2, 3, 4, 5, 5, 7, 8, 9]),
        DatasetMode.EXCHANGE4: torch.tensor([0, 1, 2, 3, 9, 5, 6, 7, 8, 9]),
        DatasetMode.EXCHANGE9: torch.tensor([0, 1, 2, 3, 4, 5, 6, 7, 8, 4]),
        DatasetMode.EXCHANGEPROB : torch.tensor([0, 7, 2, 3, 9, 6, 5, 1, 8, 4]),
    }
    # 概率交换标签时的交换概率
    prob = 0.3
    def __init__(self, train, transform, mode):
        # 加载原始数据集
        self.dataset = datasets.MNIST('DATASET',train=train,transform=transforms.ToTensor(),download=False)
        
        # 根据模式修改标签
        if mode == DatasetMode.ORIGIN:
            return
        elif mode == DatasetMode.EXCHANGEPROB:
            length = round(self.prob*self.dataset.targets.size()[0])
            self.dataset.targets = torch.cat((
                self.definelabels[mode][self.dataset.targets[:length]],
                self.dataset.targets[length:]), -1)
        else:
            self.dataset.targets = self.definelabels[mode][self.dataset.targets]
    
    # 继承Dataset类需要实现的方法，表示数据集大小
    def __len__(self):
        return len(self.dataset)
    # 继承Dataset类需要实现的方法，用于从数据集中取数据
    def __getitem__(self, index):
        return self.dataset[index]
    
'''
数据集使用方法：

# train = true时加载训练集，否则加载测试集
dt = MNISTDataset(train = True, transform = transform,mode = DatasetMode.ORIGIN)
'''

# 保存模型参数
def savedict(model, filename):
    torch.save(model.state_dict(), MODELDIR + filename + ".pt")
    
# 读取模型参数
def loaddict(filename):
    return torch.load(MODELDIR + filename + ".pt")
'''
用法

savedict(model, "mode1_1")
loaddict("moode1_1")
'''

# 绘制混淆矩阵
def confusion_matrix(preds, labels, conf_matrix):
    preds = torch.argmax(preds, 1)
    for (p, t) in zip(preds, labels):
        conf_matrix[p, t] += 1
    return conf_matrix

# 训练集加载器
train_loader = torch.utils.data.DataLoader(
    MNISTDataset(train=True, transform=transforms.ToTensor(), mode=DatasetMode.EXCHANGE9),
    batch_size=64, shuffle=True)
# 测试集加载器
test_loader = torch.utils.data.DataLoader(
    MNISTDataset(train=False, transform=transforms.ToTensor(), mode=DatasetMode.ORIGIN),
    batch_size=128, shuffle=False)


# 按普通逻辑训练，将训练数据保存到指定文件中
def normaltrain(train_loader, test_loader, num_epoch, filename, save):
    # 训练模型
    model = CNNModel()
    # 将模型放置在GPU上
    model.cuda()
    # 定义损失函数
    criterion = nn.CrossEntropyLoss()
    # 定义优化器
    optimizer = torch.optim.Adam(model.parameters(), lr=0.0002, betas=(0.5, 0.999))
    # 打开数据保存文件
    with open(RESULTDIR + filename + '.txt', 'a+', encoding='utf-8') as file:
        # 开始训练
        for epoch in range(num_epoch):
            file.write('Epoch ' + str(epoch) + '\n')
            # 训练loss
            # train_loss = 0
            # 训练集各子类准确率
            # train_correct = list(0. for i in range(10))
            # train_total = list(0. for i in range(10))

            # 当前一轮模型在训练集上的混淆矩阵
            train_confusion = torch.zeros(10, 10)

            # 训练时需要设置为训练模式
            model = model.train()

            # 从数据集加载器中读取一个batch的数据
            for img, label in train_loader:
                # 图片数据
                img = Variable(img.cuda())
                # 标签数据
                label = Variable(label.cuda())

                # 自动调用前向传播
                output = model(img)
                loss = criterion(output, label)

                # 反向传播，每个batch需要先将优化器累计梯度清零
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                # 记录每个batch上的误差
                # train_loss += loss.item()

                # 计算每个batch上的分类的准确率
                prediction = torch.argmax(output, 1)
                # res = prediction == label
                for (p, l) in zip(prediction, label):
                    train_confusion[p, l] += 1
                # 遍历计算每种图片的单体准确率
                # for label_idx in range(len(label)):
                #     label_single = label[label_idx]
                #     train_correct[label_single] += res[label_idx].item()
                #     train_total[label_single] += 1
            file.write(str(train_confusion) + '\n')
            # 测试loss
            # eval_loss = 0
            # 测试集各子类准确率
            # eval_correct = list(0. for i in range(10))
            # eval_total = list(0. for i in range(10))
            # 当前一轮模型在测试集上的混淆矩阵
            eval_confusion = torch.zeros(10, 10)
            # 测试时需要设置为测试模式
            model = model.eval()
            # 测试集不训练
            for img, label in test_loader:
                # 图片数据
                img = Variable(img.cuda())
                # 标签数据
                label = Variable(label.cuda())

                # 自动调用前向传播
                output = model(img)
                loss = criterion(output, label)

                # 记录误差
                # eval_loss += loss.item()

                # 计算每个batch上的分类的准确率
                prediction = torch.argmax(output, 1)
                for (p, l) in zip(prediction, label):
                    eval_confusion[p, l] += 1
                # res = prediction == label

                # 遍历计算每种图片的单体准确率
                # for label_idx in range(len(label)):
                #     label_single = label[label_idx]
                #     eval_correct[label_single] += res[label_idx].item()
                #     eval_total[label_single] += 1
            file.write(str(eval_confusion) + '\n')
            # 保存数据
        #     f.write(str(epoch + 1) + ',' + str(train_loss) + ',' + str(eval_loss) + ',')
        #     for i in range(10):
        #         f.write(str(train_correct[i] / train_total[i]) + ',' + str(eval_correct[i] / eval_total[i]) + ',')
        #     f.write('\n')
        #     print("Epoch {} finished".format(epoch + 1));
        # f.close()
        if save:
            savedict(model, filename)


'''
普通逻辑训练方法

normaltrain(train_loader, test_loader, 20, "origin", False)
'''
normaltrain(train_loader, test_loader, 100, "MNIST-CNN-AVOID9", True)