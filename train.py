# coding=utf-8
import math
import os
import time

from collections import OrderedDict
from enum import Enum
from random import sample

import torch
import torch.nn as nn
import torch.nn.functional as F

from torch.autograd import Variable
from torchvision import datasets, transforms
from models import CNNModel, AlexNet, VGG

# 输出结果目录名
RESULTDIR = "./RESULT/"
# 加载规避点目录名
MODELDIR = "./MODEL/"

'''
模型使用方法：

model = CNNModel()
'''

# 数据集模式
class DatasetMode(Enum):
    # 原始数据集
    ORIGIN = 0
    # 交换标签1
    EXCHANGE1 = 1
    # 交换标签7
    EXCHANGE7 = 2

    # 交换标签5
    EXCHANGE5 = 3
    # 交换标签6
    EXCHANGE6 = 4

    # 交换标签4
    EXCHANGE4 = 5
    # 交换标签9
    EXCHANGE9 = 6

    # 概率交换标签
    EXCHANGEPROB = 7

# 按需求生成的数据集
class MNISTDataset(torch.utils.data.Dataset):
    # 数据集模式对应的标签更改
    definelabels = {
        DatasetMode.EXCHANGE1 : torch.tensor([0, 7, 2, 3, 4, 5, 6, 7, 8, 9]),
        DatasetMode.EXCHANGE7: torch.tensor([0, 1, 2, 3, 4, 5, 6, 1, 8, 9]),
        DatasetMode.EXCHANGE5: torch.tensor([0, 1, 2, 3, 4, 6, 6, 7, 8, 9]),
        DatasetMode.EXCHANGE6: torch.tensor([0, 1, 2, 3, 4, 5, 5, 7, 8, 9]),
        DatasetMode.EXCHANGE4: torch.tensor([0, 1, 2, 3, 9, 5, 6, 7, 8, 9]),
        DatasetMode.EXCHANGE9: torch.tensor([0, 1, 2, 3, 4, 5, 6, 7, 8, 4]),
        DatasetMode.EXCHANGEPROB : torch.tensor([0, 7, 2, 3, 9, 6, 5, 1, 8, 4]),
    }
    # 概率交换标签时的交换概率
    prob = 0.3
    def __init__(self, train, transform, mode):
        # 加载原始数据集
        self.dataset = datasets.MNIST('DATASET',train=train,transform=transforms.ToTensor(),download=False)

        # 根据模式修改标签
        if mode == DatasetMode.ORIGIN:
            return
        elif mode == DatasetMode.EXCHANGEPROB:
            length = round(self.prob*self.dataset.targets.size()[0])
            self.dataset.targets = torch.cat((
                self.definelabels[mode][self.dataset.targets[:length]],
                self.dataset.targets[length:]), -1)
        else:
            self.dataset.targets = self.definelabels[mode][self.dataset.targets]

    # 继承Dataset类需要实现的方法，表示数据集大小
    def __len__(self):
        return len(self.dataset)
    # 继承Dataset类需要实现的方法，用于从数据集中取数据
    def __getitem__(self, index):
        return self.dataset[index]

'''
数据集使用方法：

# train = true时加载训练集，否则加载测试集
dt = MNISTDataset(train = True, transform = transform,mode = DatasetMode.ORIGIN)
'''

# 保存模型参数
def savedict(model, filename):
    torch.save(model.state_dict(), MODELDIR + filename + ".pt")

# 读取模型参数
def loaddict(filename):
    return torch.load(MODELDIR + filename + ".pt")
'''
用法

savedict(model, "mode1_1")
loaddict("moode1_1")
'''

# 绘制混淆矩阵
def confusion_matrix(preds, labels, conf_matrix):
    preds = torch.argmax(preds, 1)
    for (p, t) in zip(preds, labels):
        conf_matrix[p, t] += 1
    return conf_matrix

# 训练集加载器
train_loader = torch.utils.data.DataLoader(
    MNISTDataset(train=True, transform=transforms.ToTensor(), mode=DatasetMode.EXCHANGEPROB),
    batch_size=64, shuffle=True)
# 测试集加载器
test_loader = torch.utils.data.DataLoader(
    MNISTDataset(train=False, transform=transforms.ToTensor(), mode=DatasetMode.ORIGIN),
    batch_size=128, shuffle=False)

# 加载规避点
def loadavoidlist():
    # 规避点集合
    avoidlist = []
    # 加载规避点OrderedDict集合
    filelist = os.listdir(MODELDIR)
    for filename in filelist:
        splitname = os.path.splitext(filename)
        if splitname[1] == ".pt":
            avoidlist.append(loaddict(splitname[0]))
    # 预处理
    for avoidpoint in avoidlist:
        for k, v in avoidpoint.items():
            if ("weight" in k) or ("bias" in k):
                avoidpoint[k] = torch.flatten(v).view(1, -1)
    return avoidlist

# 过滤函数
def filterfunc(avoidlist, model, delta):
    # 预处理
    modeldict = {}
    for k, v in model.state_dict().items():
        if ("weight" in k) or ("bias" in k):
            modeldict[k] = torch.flatten(v).view(1, -1).cuda()
    # 欧氏距离函数
    euclidean = nn.PairwiseDistance(p=2.0)
    # 遍历每一个规避点
    for avoidpoint in avoidlist:
        # 欧式距离的平方
        eu_value = 0
        # 计算某一层欧氏距离再平方，进行累加
        for k, v in avoidpoint.items():
            if ("weight" in k) or ("bias" in k):
                eu_value += euclidean(v.cuda(), modeldict[k]).item() ** 2
        if (eu_value ** 0.5) < delta:
            return False
    return True

# 按新逻辑训练
def improvedtrain(train_loader, test_loader, num_epoch, population_size, delta, alpha, filename, save):
    # 加载规避点
    avoidlist = loadavoidlist()
    # 定义损失函数
    criterion = nn.CrossEntropyLoss()

    # 根据population_size大小生成初始种群
    models = []
    while(len(models) < population_size):
        model = CNNModel()
        # 通过过滤函数
        if(filterfunc(avoidlist, model, delta)):
            models.append(model)

    # 打开数据保存文件
    with open(RESULTDIR + filename + '.txt', 'a+', encoding='utf-8') as file:

        # 对每个epoch的训练
        start_time = time.time()
        for epoch in range(num_epoch):
            file.write('Epoch ' + str(epoch) + '\n')
            modelscount = len(models)
            # 当前一轮各模型的训练loss
            # train_loss = list(0. for i in range(modelscount))
            # 当前一轮各模型对各子类训练集上的准确率
            # train_correct = list(list(0. for i in range(10)) for j in range(modelscount))
            # train_total = list(0. for i in range(10))
            # 当前一轮各模型在训练集上的混淆矩阵
            train_confusions = []
            for i in range(modelscount):
                train_confusions.append(torch.zeros(10, 10))
            # 从数据集加载器中读取一个batch的数据
            for img, label in train_loader:
                # 图片数据
                img = Variable(img.cuda())
                # 标签数据
                label = Variable(label.cuda())
                # 统计当前batch中各类数目
                # for label_idx in range(len(label)):
                #     train_total[label[label_idx]] += 1

                optimizers = []
                # 为每个模型配备优化器
                for model in models:
                    optimizers.append(torch.optim.Adam(model.parameters(), lr=0.0002, betas=(0.5, 0.999)))

                ktemp = []
                kcross = []
                kselect = []

                # 每个模型进行一个step的训练，维持ktemp
                for i, (model, optimizer) in enumerate(zip(models, optimizers)):
                    # 训练时需要设置为训练模式
                    model.cuda()
                    model.train()
                    # 前向传播
                    output = model(img)
                    loss = criterion(output, label)

                    # 反向传播
                    optimizer.zero_grad()
                    loss.backward()
                    optimizer.step()

                    # 加入ktemp
                    ktemp.append(model)

                    # 记录误差
                    # train_loss[i] += loss.item()

                    # 记录分类准确率
                    prediction = torch.argmax(output, 1)
                    # res = prediction == label
                    for (p, l) in zip(prediction, label):
                        train_confusions[i][p, l] += 1

                    # 遍历计算每种图片的单体准确率
                    # for label_idx in range(len(label)):
                    #     label_single = label[label_idx]
                    #     train_correct[i][label_single] += res[label_idx].item()

                # 此时ktemp已准备好，接下来产生kcross
                crossdict = OrderedDict()
                for i in range(len(ktemp)):
                    for j in range(i):
                        for (k1, v1), (k2, v2) in zip(ktemp[i].state_dict().items(),ktemp[j].state_dict().items()):
                            crossdict[k1] = (v1 + v2) / 2
                        modeltmp = CNNModel()
                        modeltmp.load_state_dict(crossdict)
                        kcross.append(modeltmp)

                # 此时kcross已经准备好，接下来产生kselect
                for kmodel in kcross:
                    if(filterfunc(avoidlist, kmodel, delta)):
                        kselect.append(kmodel)
                while(len(kselect) < math.floor((1-alpha) * modelscount)):
                    tmp = sample(ktemp, math.floor(alpha * modelscount))
                    for kmodel in tmp:
                        if(filterfunc(avoidlist, kmodel, delta)):
                            kselect.append(kmodel)

                # TODO？？？

                # 使用kselect和ktemp来更新训练模型列表
                models = sample(kselect, math.floor(alpha * modelscount))
                models.extend(sample(ktemp, modelscount - math.floor(alpha * modelscount)))

            modelscount = len(models)
            # 当前一轮各模型的训练loss
            # eval_loss = list(0. for i in range(modelscount))
            # 当前一轮各模型对各子类测试集上的准确率
            # eval_correct = list(list(0. for i in range(10)) for j in range(modelscount))
            # eval_total = list(0. for i in range(10))

            # 当前一轮各模型在测试集上的混淆矩阵
            eval_confusions = []
            for i in range(modelscount):
                eval_confusions.append(torch.zeros(10, 10))

            # 从测试集中加载一个batch的数据
            for img, label in test_loader:
                # 图片数据
                img = Variable(img.cuda())
                # 标签数据
                label = Variable(label.cuda())
                # 统计当前batch中各类数目
                # for label_idx in range(len(label)):
                #     eval_total[label[label_idx]] += 1

                for i, model in enumerate(models):
                    # 测试时需要设置为测试模式
                    model.cuda()
                    model = model.eval()

                    # 前向传播
                    output = model(img)
                    # loss = criterion(output,label)

                    # 记录误差
                    # eval_loss[i] += loss.item()

                    # 记录分类准确率
                    prediction = torch.argmax(output, 1)
                    # res = prediction == label
                    for (p, l) in zip(prediction, label):
                        eval_confusions[i][p, l] += 1

                    # 遍历计算每种图片的单体准确率
                    # for label_idx in range(len(label)):
                    #     label_single = label[label_idx]
                    #     eval_correct[i][label_single] += res[label_idx].item()
            # 保存数据
            for i in range(modelscount):
                file.write('ModelTrain ' + str(i) + '\n')
                file.write(str(train_confusions[i]) + '\n')
                file.write('ModelEval ' + str(i) + '\n')
                file.write(str(eval_confusions[i]) + '\n')
            # for i in range(modelscount):
            #     print(str(epoch+1)+','+"Model"+str(i)+','+str(train_loss[i])+','+str(eval_loss[i])+',')
            #     for j in range(10):
            #         print(str(train_correct[i][j]/train_total[j])+','+str(eval_correct[i][j]/eval_total[j])+',')
            #     print('\n')
            print("Epoch {} finished".format(epoch+1));

        end_time = time.time()
        print(end_time - start_time)

        if save:
            savedict(model, filename)

'''
新算法使用方法

improvedtrain(train_loader, test_loader, num_epoch, population_size, delta, alpha, filename, save):
'''

improvedtrain(train_loader, test_loader, 100, 10, 5, 0.2, "MNIST-delta5-CNN-new", True)