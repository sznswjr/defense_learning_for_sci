# coding=utf-8

import itertools
import numpy as np
import matplotlib.pyplot as plt

# 输出结果所在目录名
RESULTDIR = "./RESULT/"

# 读出一个混淆矩阵
def stringToConfusionMatrix(file):
    # 待填充的矩阵
    confusionMatrix = np.zeros((10, 10), dtype = np.int)
    # 一个Tensor有20行
    tensorLines = 0
    while tensorLines < 10:
        lineStr = file.readline()
        if lineStr.find(']') == -1:
            lineStr += file.readline()
        lineStr = "".join(lineStr.split())
        start = lineStr.find('tensor([[')
        if start == 0:
            start = 9
        else:
            start = 1
        end = lineStr.find(']')
        confusionMatrix[tensorLines] = np.array(lineStr[start: end].split(','), dtype=np.float)
        tensorLines += 1
    return confusionMatrix

# 混淆矩阵可视化
def confusionMatrixToPicture(confusionMatrix, classLabels, filename, normalize=False, title='', cmap=plt.cm.Blues):
    plt.figure()
    plt.imshow(confusionMatrix, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classLabels))
    plt.xticks(tick_marks, classLabels, rotation=90)
    plt.yticks(tick_marks, classLabels)

    plt.axis("equal")

    ax = plt.gca()
    left, right = plt.xlim()
    ax.spines['left'].set_position(('data', left))
    ax.spines['right'].set_position(('data', right))
    for edge_i in ['top', 'bottom', 'right', 'left']:
        ax.spines[edge_i].set_edgecolor("white")

    thresh = confusionMatrix.max() / 2.
    for i, j in itertools.product(range(confusionMatrix.shape[0]), range(confusionMatrix.shape[1])):
        num = '{:.2f}'.format(confusionMatrix[i, j]) if normalize else int(confusionMatrix[i, j])
        plt.text(j, i, num,
                 verticalalignment='center',
                 horizontalalignment="center",
                 color="white" if num > thresh else "black")

    # plt.ylabel('Self patt')
    # plt.xlabel('Transition patt')

    plt.tight_layout()
    plt.savefig(RESULTDIR + filename + '.png', transparent=True, dpi=800)
    plt.close()
    # plt.show()
'''
使用方法：
confusionMatrixToPicture(confusionMatrix, ["Num {}".format(i) for i in range(1, 11)], "delta5")
'''

# 根据混淆矩阵计算总accuracy
def calculateTotalAccuracy(confusionMatrix):
    totalCount = confusionMatrix.sum()
    correctCount = (np.diag(confusionMatrix)).sum()
    accuracy = round(100*float(correctCount)/float(totalCount), 2)
    return accuracy

# 根据混淆矩阵计算各类的accuracy
def calculateLabelAccuracy(confusionMatrix):
    labelCounts = []
    for i in range(10):
        labelCounts.append(confusionMatrix.sum(axis=0)[i])
    correctCounts = []
    for i in range(10):
        correctCounts.append(confusionMatrix[i][i])
    accuracys = []
    for i in range(10):
        accuracys.append(round(100*float(correctCounts[i])/float(labelCounts[i]),2))
    return accuracys

# 根据混淆矩阵计算各类的recall
def calculateLabelRecall(confusionMatrix):
    labelCounts = []
    for i in range(10):
        labelCounts.append(confusionMatrix.sum(axis=1)[i])
    correctCounts = []
    for i in range(10):
        correctCounts.append(confusionMatrix[i][i])
    recalls = []
    for i in range(10):
        if labelCounts[i] == 0:
            recalls.append(0)
        else:
            recalls.append(round(100 * float(correctCounts[i]) / float(labelCounts[i]), 2))
    return recalls

# 根据混淆矩阵计算各类的precision
def calculateLabelPrecision(confusionMatrix):
    labelCounts = []
    for i in range(10):
        labelCounts.append(confusionMatrix.sum(axis=0)[i])
    correctCounts = []
    for i in range(10):
        correctCounts.append(confusionMatrix[i][i])
    precisions = []
    for i in range(10):
        if labelCounts[i] == 0:
            precisions.append(0)
        else:
            precisions.append(round(100 * float(correctCounts[i]) / float(labelCounts[i]), 2))
    return precisions

# 根据混淆矩阵计算各类的f1-score
def calculateLabelF1score(confusionMatrix):
    F1scores = []
    accuracys = calculateLabelAccuracy(confusionMatrix)
    recalls = calculateLabelRecall(confusionMatrix)
    for i in range(10):
        if (accuracys[i] + recalls[i]) == 0:
            F1scores.append(0)
        else:
            F1scores.append(round(2*accuracys[i]*recalls[i]/(accuracys[i]+recalls[i]), 2))
    return F1scores

# 输出指标数据
def confusionMatrixToMeasurement(confusionMatrix, filename, i, j = 0):
    with open(filename + '.txt', 'a+', encoding='utf-8') as file:
        file.write('Epoch ' + str(i) + 'Model ' + str(j) + '\n')
        file.write("total accuracy:" + str(calculateTotalAccuracy(confusionMatrix)) + '\n')
        file.write("label accuracy:" + str(calculateLabelAccuracy(confusionMatrix)) + '\n')
        file.write("label precision:" + str(calculateLabelPrecision(confusionMatrix)) + '\n')
        file.write("label recall:" + str(calculateLabelRecall(confusionMatrix)) + '\n')
        file.write("label f1score:" + str(calculateLabelF1score(confusionMatrix)) + '\n')

# 处理改进训练得到的数据文件
def processImprovedTrain(filepath):
    with open(filepath, 'r', encoding='utf-8') as file:
        '''
        文件格式：
        for i in range(100):
            Epoch i
            for j in range(10):
                ModelTrain j 后面有20行
                ModelEval j 每个后面有20行
        '''
        for i in range(100):
            # Epoch i
            file.readline()
            for j in range(10):
                # ModelTrain j
                file.readline()
                _ = stringToConfusionMatrix(file)
                # ModelEval j
                file.readline()
                confusionMatrix = stringToConfusionMatrix(file)
                confusionMatrixToMeasurement(confusionMatrix, RESULTDIR + "data", i, j)
                # confusionMatrixToPicture(confusionMatrix, [format(classes[j]) for j in range(0, 10)], "pic-" + str(i) + '-' + str(j))
classesCifar = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
classes = ('Num 0', 'Num 1', 'Num 2', 'Num 3',
           'Num 4', 'Num 5', 'Num 6', 'Num 7', 'Num 8', 'Num 9')
# 处理原始训练得到的数据文件
def processOriginTrain(filepath):
    with open(filepath, 'r', encoding='utf-8') as file:
        '''
        文件格式：
        for i in range(100):
            Epoch i 后面有40行
        '''
        for i in range(100):
            file.readline()
            _ = stringToConfusionMatrix(file)
            confusionMatrix = stringToConfusionMatrix(file)
            confusionMatrixToMeasurement(confusionMatrix, RESULTDIR + "origin-data", i)
            confusionMatrixToPicture(confusionMatrix, [format(classes[j]) for j in range(0, 10)], "origin-pic-" + str(i))

# with open(r'RESULT/MNIST-delta5-CNN-new.txt', 'r', encoding='utf-8') as file:
#     _ = stringToConfusionMatrix(file)
#     confusionMatrix = stringToConfusionMatrix(file)
#     print("total accuracy:", calculateTotalAccuracy(confusionMatrix))
#     print("label accuracy:", calculateLabelAccuracy(confusionMatrix))
#     print("label precision:", calculateLabelPrecision(confusionMatrix))
#     print("label recall:", calculateLabelRecall(confusionMatrix))
#     print("label f1score:", calculateLabelF1score(confusionMatrix))
#     confusionMatrixToPicture(confusionMatrix, ["Num {}".format(i) for i in range(0, 10)], "delta5")

# processOriginTrain(r'RESULT/VGGCifar10-prob.txt')
processImprovedTrain(r'RESULT/MNIST-delta5-CNN-new.txt')